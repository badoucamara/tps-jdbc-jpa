# R�ponses aux questions


####Serie 1

**Exercice 3**

__Q1 :__  Le nombre de maires en base est 35886.

__Q2 :__ Le nombre de maires dont la civilit� Mme dans la table est 5670.

__Q3 :__ Le maire le plus ancien dans la table est M. Marcel BERTHOME 1922-04-04

__Q5 :__ La population totale de toutes les communes est 64347610.

__Q6 :__ Le nombre de maires n�s en 1944 dans la table est 717.





####Serie 2

**Exercice 4**

__Q2 :__ 

a) D'un c�t�, un musicien peut jouer � plusieurs instruments. D'un autre c�t�, un instrument peut �tre jou� par plusieurs musiciens. On a donc une relation de type *N:P* 

b) Sachant que les collections associ�es aux entit�s (Musicien ou Instrument) ne contient pas de doublons, on peut utiliser un *SET* afin d'impl�menter cette relation.

__Q3 :__ 

a) Sachant qu'un Professeur enseigne un instrument unique. Il s'agit d'une relation *1:1*.

b) Sachant qu'un professeur est un musicien, la classe Professeur *h�rite* donc de la classe Musicien.  Pour mapper cette relation, on peut utiliser la strat�gie de mappage JOINED.

__Q4 :__ 

a) Le type est la relation entre un orchestre et ses musiciens est une relation *1:p*.

__Q5 :__ 

a) La relation entre un concert et l�orchestre qui donne ce concert est une relation de type *1:1*.

####Serie 3

**Exercice 5**

__Q2 :__ 

a) Le tpe de relation entre Maire et Commune est une relation de type *1:1*
