package org.camara.serie2.exercice4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.camara.serie2.exercice4.model.Concert;
import org.camara.serie2.exercice4.model.Instrument;
import org.camara.serie2.exercice4.model.Musicien;
import org.camara.serie2.exercice4.model.Orchestre;
import org.camara.serie2.exercice4.model.Professeur;
import org.camara.serie2.exercice4.model.utils.Civilite;
import org.camara.serie2.exercice4.model.utils.TypeInstrument;

public class Main {

	private static Map<String, Instrument> registryInstruments = new HashMap<>();
	private static Map<String, Musicien> registryMusiciens = new HashMap<>();
	private static Map<String, Orchestre> registryOrchestres = new HashMap<>();
	
	public static void main(String[] args) {
		readInstruments();
 
		List<Musicien> musiciens = readMusiciens();
		List<Professeur> profs = readProfs();

		List<String> musicienNames = musiciens.stream().map(m -> m.getNom()).collect(Collectors.toList());

		/* Elimination des musiciens qui sont profs pour �viter leur double insertion */
		List<Musicien> musicienSansProf = musiciens.stream().filter(m -> musicienNames.contains(m.getNom()))
				.collect(Collectors.toList());

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("tp-jpa");
		EntityManager entityManager = emf.createEntityManager();

		entityManager.getTransaction().begin();
		musicienSansProf.forEach(entityManager::persist);
		entityManager.getTransaction().commit();
		System.out.println("Musicians persisted with success  !!");

		entityManager.getTransaction().begin();
		profs.forEach(entityManager::persist);
		entityManager.getTransaction().commit();
		System.out.println("Professors persisted with success !!");

		List<Orchestre> orchestres = readOrchestres();
		orchestres.forEach(o -> {
			if (o == null)
				orchestres.remove(o);
			
		});

		entityManager.getTransaction().begin();
		orchestres.forEach(entityManager::persist);
		entityManager.getTransaction().commit();
		System.out.println("Orchestres persisted with success !!");
		
		
		buildProgramme();
	}

	public static List<Instrument> readInstruments() {

		List<Instrument> instruments = List.of();
		Function<String, Instrument> lineToInstrument = line -> {

			String[] split = line.split("[ ]+");
			TypeInstrument type = TypeInstrument.of(split[1]);
			Instrument instrument = new Instrument(split[0], type);
			registryInstruments.put(split[0], instrument);
			return instrument;

		};

		Path path = Path.of("data/instruments.txt");

		try (Stream<String> lines = Files.newBufferedReader(path).lines();) {

			instruments = lines.filter(line -> !line.startsWith("#")).map(lineToInstrument)
					.collect(Collectors.toList());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return instruments;

	}

	public static List<Musicien> readMusiciens() {
		List<Musicien> musiciens = List.of();

		Function<String, Musicien> lineToMusicien = line -> {

			String[] split = line.split("[ ]+");
			String[] instruments = Arrays.copyOfRange(split, 2, split.length);
			Civilite civilite = Civilite.of(split[0]);
			Musicien musicien = new Musicien(split[1], civilite);
			registryMusiciens.put(split[1], musicien);

			Arrays.stream(instruments).map(nomInstrument -> registryInstruments.get(nomInstrument))
					.forEach(musicien::addInstrument);

			return musicien;

		};

		Path path = Path.of("data/musiciens.txt");

		try (Stream<String> lines = Files.newBufferedReader(path).lines();) {

			musiciens = lines.filter(line -> !line.startsWith("#")).map(lineToMusicien).collect(Collectors.toList());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return musiciens;
	}

	public static List<Professeur> readProfs() {

		List<Professeur> profs = List.of();
		Function<String, Professeur> lineToProf = line -> {
			Professeur prof = null;
			String[] split = line.split("[ ]+");

			Musicien musicien = registryMusiciens.get(split[0]);

			if (musicien != null) {
				prof = new Professeur(musicien.getNom(), musicien.getCivilite(), musicien.getInstruments(),
						registryInstruments.get(split[1]));
			}
			return prof;
		};

		Path path = Path.of("data/professeurs.txt");

		try (Stream<String> lines = Files.newBufferedReader(path).lines();) {

			profs = lines.filter(line -> !line.startsWith("#")).map(lineToProf).collect(Collectors.toList());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return profs;

	}

	public static List<Orchestre> readOrchestres() {

		File file = new File("data/orchestres.txt");
		List<Orchestre> orchestres = new ArrayList<Orchestre>();

		try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr);) {

			String line = br.readLine();

			while (line != null) {
				if (line.startsWith("#")) {

					Orchestre o = new Orchestre();
					line = br.readLine();
					o.setNom(line.split("[ ]+")[1]);
					line = br.readLine();
					line = br.readLine();
					String[] musiciens = line.split(", ");
					Arrays.asList(musiciens).forEach(m -> {
						o.addMusicen(registryMusiciens.get(m));
					});

					orchestres.add(o);
					registryOrchestres.put(o.getNom(), o);

				}
				line = br.readLine();
			}

		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orchestres;
	}

	public static void buildProgramme() {

		System.out.println("*********************************************************************************");
		System.out.println();

		Concert concert = new Concert();
		Path path = Path.of("data/concert.txt");

		try (Stream<String> lines = Files.newBufferedReader(path).lines();) {

			List<String> list = lines.collect(Collectors.toList());
			list.forEach(l -> {
				if (l.startsWith("#")) {
					System.out.println(" \t \t Bienvenue au" + l.split("#")[1]);
				}
				
				else if (l.startsWith("Date")) {
					concert.setDate(l.split(":")[1]);
					System.out.println();
					System.out.println(" \t \t Date du concert : Le" +concert.getDate());
				}
				
				else if (l.startsWith("Lieu")) {
					concert.setLieu(l.split(":")[1]);
					System.out.println();
					System.out.println(" \t \t Lieu du concert : Le" +concert.getLieu());
				}
				
				else if (l.startsWith("Orchestre")) {
					
					concert.setOrchestre(registryOrchestres.get(l.split(" ")[1]));
					System.out.println();
					System.out.println(" \t \t Orchestre pr�sent : Orchestre " +concert.getOrchestre().getNom());
					System.out.println();
					System.out.println(" \t \t Musicens pr�sents : ");
					concert.getOrchestre().getMusiciens().forEach(m->{
				if(m!=null) {
					
					Instrument first = m.getInstruments().stream().filter(i->i!=null).findFirst().get();
					System.out.println(" \t \t \t " + m.getNom() + "   ==> Instrument : " + first.getNom());
				}
						
					});
				}
				
			});

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
