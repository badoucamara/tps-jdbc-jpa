package org.camara.serie2.exercice4.model.utils;

import java.util.Arrays;

public enum TypeInstrument {
	CORDES("cordes"), VENT("vent"), BOIS("bois"), CUIVRE("cuivre"), CORDE_PINCEES("cordes pinc�es"),
	CORDE_FRAPPEES("cordes frapp�es"), PERCUSSION("percussion");

	private String label;

	private TypeInstrument(String label) {
		this.label = label;
	}

	public static TypeInstrument of(String label) {

		return Arrays.stream(values()).filter(value -> value.label.equals(label)).findFirst().orElseThrow();

	}

}
