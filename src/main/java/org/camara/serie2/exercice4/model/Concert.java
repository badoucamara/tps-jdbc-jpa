package org.camara.serie2.exercice4.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity

public class Concert implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String date;
	private String lieu;

	
	private Orchestre orchestre;
	
	
	public Orchestre getOrchestre() {
		return orchestre;
	}



	public void setOrchestre(Orchestre orchestre) {
		this.orchestre = orchestre;
	}



	public Concert () {
	}



	public String getDate() {
		return date;
	}



	public void setDate(String date) {
		this.date = date;
	}



	public Concert( String date, String lieu) {
		this.date = date;
		this.lieu = lieu;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getLieu() {
		return lieu;
	}



	public void setLieu(String lieu) {
		this.lieu = lieu;
	}



	@Override
	public String toString() {
		return "Concert [id=" + id + ", date=" + date + ", lieu=" + lieu + "]";
	}
	
	
	
	
}
