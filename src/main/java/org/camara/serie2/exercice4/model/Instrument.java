package org.camara.serie2.exercice4.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.camara.serie2.exercice4.model.utils.TypeInstrument;

@Entity
public class Instrument implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	private String nom;
	@Enumerated(value = EnumType.STRING)
	private TypeInstrument type;

	public Instrument() {
	}

	public Instrument(String nom, TypeInstrument type) {
		this.nom = nom;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public TypeInstrument getType() {
		return type;
	}

	public void setType(TypeInstrument type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Instrument [id=" + id + ", nom=" + nom + ", type=" + type + "]";
	}
	

}
