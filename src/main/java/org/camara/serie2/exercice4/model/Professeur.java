package org.camara.serie2.exercice4.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.camara.serie2.exercice4.model.utils.Civilite;

@Entity
public class Professeur extends Musicien {

	private static final long serialVersionUID = 1L;
	
	@OneToOne
	private Instrument instrumentEnseigne;

	public Professeur() {

	}

	public Professeur(Instrument instrument) {
		this.instrumentEnseigne = instrument;
	}

	public Instrument getInstrument() {
		return instrumentEnseigne;
	}

	public void setInstrument(Instrument instrument) {
		this.instrumentEnseigne = instrument;
	}

	

	@Override
	public String toString() {
		return "Professeur [instrumentEnseigne=" + instrumentEnseigne + ", getNom()=" + getNom() + ", getInstruments()="
				+ getInstruments() + "] \n";
	}

	public Professeur(String nom, Civilite civilite, Set<Instrument> instruments,Instrument instrumentEnseigne) {
		super(nom, civilite, instruments);
		this.instrumentEnseigne = instrumentEnseigne;
	}

}
