package org.camara.serie2.exercice4.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Orchestre implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;

	private String nom;

	
	@ManyToMany(cascade = CascadeType.PERSIST)
	private List<Musicien> musiciens = new ArrayList<>();

	public Orchestre() {
	}

	@Override
	public String toString() {
		return "Orchestre [id=" + id + ", nom=" + nom + ", musiciens=" + musiciens + "]";
	}

	public Map<Instrument, List<Musicien>> getInstruments()  {

		Map<Instrument, List<Musicien>> map = new HashMap<>();

		for (Musicien m : musiciens) {
			
			Set<Instrument> instruments = new HashSet<Instrument>();
				instruments = m.getInstruments();
				System.out.println(instruments);
			instruments.forEach(i -> {

				if (!map.containsKey(i)) {
					List<Musicien> musiciens = new ArrayList<>();
					musiciens.add(m);
					map.put(i, musiciens);
				}

				else {
					List<Musicien> oldList = map.get(i);
					oldList.add(m);
					map.replace(i, oldList);
				}
			});
		}
		return map;

	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Musicien> getMusiciens() {
		return musiciens;
	}

	public void setMusiciens(List<Musicien> musiciens) {
		this.musiciens = musiciens;
	}

	public void addMusicen (Musicien m) {
		this.musiciens.add(m);
		
	}
	
	
	
	
	
}
