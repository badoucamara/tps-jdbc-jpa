package org.camara.serie2.exercice4.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.camara.serie2.exercice4.model.utils.Civilite;

@Inheritance (strategy = InheritanceType.JOINED)
@Entity
public class Musicien implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;

	private String nom;

	@Enumerated(value = EnumType.STRING)
	private Civilite civilite;

	@ManyToMany(cascade = CascadeType.PERSIST)
	private Set<Instrument> instruments = new HashSet<>();
	
	@ManyToMany(cascade = CascadeType.PERSIST)
	private Set<Orchestre> orchestres = new HashSet<>();

	public Musicien() {
	}

	public Musicien(String nom, Civilite civilite) {
		this.nom = nom;
		this.civilite = civilite;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public boolean addInstrument(Instrument instrument) {
		return this.instruments.add(instrument);
	}

	@Override
	public String toString() {
		return "Musicien [id=" + id + ", nom=" + nom + ", civilite=" + civilite + ", instruments=" + instruments + "]";
	}

	public Musicien(String nom, Civilite civilite, Set<Instrument> instruments) {
		this.nom = nom;
		this.civilite = civilite;
		this.instruments = instruments;
	}

	public Set<Instrument> getInstruments() {
		return instruments;
	}

	public void setInstruments(Set<Instrument> instruments) {
		this.instruments = instruments;
	}

}
