package org.camara.serie2.exercice4.model.utils;

import java.util.Arrays;

public enum Civilite {

	MR("MR"), MRS("MRS");

	private String label;

	private Civilite(String label) {
		this.label = label;
	}

	public static Civilite of(String label) {
		return Arrays.stream(values()).filter(value -> value.label.equals(label)).findFirst().orElseThrow();

	}
}
