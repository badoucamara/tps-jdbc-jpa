package org.camara.serie3.exercice5.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.camara.serie3.exercice5.model.utils.Civilite;

@Entity
public class Maire implements Serializable{

	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	
	@Column(length = 80)
	private String lastName;
	
	@Column(length = 80)
	private String firstName;
	
	@Column(length = 5)
	@Enumerated(EnumType.STRING)
	private Civilite civility;
	
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;
	
	@OneToOne(mappedBy = "maire")
	private Commune commune;
	
	
	
	public Maire(int id, String lastName, String firstName, Civilite civility, Date dateOfBirth) {
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.civility = civility;
		this.dateOfBirth = dateOfBirth;
	}



	public Maire() {
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public Civilite getCivility() {
		return civility;
	}



	public void setCivility(Civilite civility) {
		this.civility = civility;
	}



	public Date getDateOfBirth() {
		return dateOfBirth;
	}



	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}



	@Override
	public String toString() {
		return "Maire [id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + ", civility=" + civility
				+ ", dateOfBirth=" + dateOfBirth + "]";
	}
	
	
	
	
	
}
