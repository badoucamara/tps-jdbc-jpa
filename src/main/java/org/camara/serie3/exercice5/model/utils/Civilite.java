package org.camara.serie3.exercice5.model.utils;

import java.util.Arrays;
import java.util.NoSuchElementException;

public enum Civilite {

	MR("M"), MME("Mme"), MLLE("Mlle");

	private String label;

	private Civilite(String label) {
		this.label = label;
	}

	public static Civilite of(String label) {
		return Arrays.stream(values()).filter(value -> value.label.equals(label)).
				findFirst().orElseThrow(()->new NoSuchElementException(label));

	}
}
