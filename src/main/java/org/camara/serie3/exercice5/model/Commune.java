package org.camara.serie3.exercice5.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Commune implements Serializable{
	
	@Id
	private String codePostal;
	private String nom;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	private Maire maire;
	
	


	public Commune() {
	}
	
	
	public Commune(String codePostal, String nom) {
		this.codePostal = codePostal;
		this.nom = nom;
	}


	public String getCodePostal() {
		return codePostal;
	}


	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public Maire getMaire() {
		return maire;
	}


	public void setMaire(Maire maire) {
		this.maire = maire;
	}

	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	@Override
	public String toString() {
		return "Commune [codePostal=" + codePostal + ", nom=" + nom + ", maire=" + maire + "]";
	}



	

}
