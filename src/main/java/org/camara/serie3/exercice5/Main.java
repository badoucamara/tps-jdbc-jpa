package org.camara.serie3.exercice5;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.camara.serie3.exercice5.model.Commune;
import org.camara.serie3.exercice5.model.Maire;
import org.camara.serie3.exercice5.model.utils.Civilite;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("tp-jpa");
		EntityManager entityManager = emf.createEntityManager();

		Map<String, Maire> maires = readMaire("data/maires-25-04-2014.csv");
		Map<String, Commune> communes = readcommunes("data/maires-25-04-2014.csv");
		
		
		for(Commune commune :communes.values()) {
			
			Maire maire = maires.get(commune.getCodePostal());
			commune.setMaire(maire);
		}
		
		entityManager.getTransaction().begin();
		communes.values().forEach(entityManager::persist);
		entityManager.getTransaction().commit();
		System.out.println( "Persisted with success  !!");

	}

	public static Map<String, Maire> readMaire(String filename) {
		Map<String, Maire> maires = new HashMap<>();

		Path path = Path.of(filename);
		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String line = reader.readLine();
			line = reader.readLine();
			while (line != null) {
				String split[] = line.split(";");

				DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
				Date date = dateformat.parse(split[8]);
				Maire maire = new Maire();
				maire.setCivility(Civilite.of(split[7]));
				maire.setFirstName(split[6]);
				maire.setLastName(split[5]);
				maire.setDateOfBirth(date);

				Maire prev = maires.put(readCodePostal(line), maire);
				if (prev != null) {
					System.out.println("Doublon =" + prev);
				}

				line = reader.readLine();

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return maires;

	}

	private static String readCodePostal(String line) {
		String[] fields = line.split(";");
		String codeDpt = fields[0];
		if (codeDpt.length() == 1)
			codeDpt = "0" + codeDpt;

		String codeInsee = fields[2];

		if (codeInsee.length() == 1)
			codeInsee = "00" + codeInsee;
		else if (codeInsee.length() == 2)
			codeInsee = "0" + codeInsee;
		return codeDpt + codeInsee;
	}

	public static Map<String, Commune> readcommunes(String filename) {

		Map<String, Commune> communes = new HashMap<>();

		Path path = Path.of(filename);
		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String line = reader.readLine();
			line = reader.readLine();
			while (line != null) {
				String split[] = line.split(";");

				Commune commune = new Commune();
				commune.setNom(split[3]);
				commune.setCodePostal(readCodePostal(line));

				Commune prev = communes.put(readCodePostal(line), commune);
				if (prev != null) {
					System.out.println("Doublon =" + prev);
				}

				line = reader.readLine();

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return communes;

	}

}
