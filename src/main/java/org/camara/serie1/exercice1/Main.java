package org.camara.serie1.exercice1;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.camara.serie1.model.Commune;
import org.camara.serie1.model.Maire;

public class Main {
	public static void main(String[] args) throws SQLException {

		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "jdbc-user",
				"badoucamara");
		String sqlMaire = "insert into Maire (first_name,last_name,civility,date_of_birth,code_insee)values (?,?,?,?,?)";
		String sqlCommune = "insert into Commune (code_postal,libelle)values (?,?)";

		PreparedStatement prepareStatementMaire = connection.prepareStatement(sqlMaire);
		PreparedStatement prepareStatementCommune = connection.prepareStatement(sqlCommune);

		Map<String, String> communes = new HashMap<>();
		Path path = Path.of("data/maires-25-04-2014.csv");
		try (BufferedReader reader = Files.newBufferedReader(path);) {
			String line = reader.readLine();
			line = reader.readLine();
			while (line != null) {

				// From Line to Maire
				Maire m = linetoMaire.apply(line);
				prepareStatementMaire.setString(1, m.getName());
				prepareStatementMaire.setString(2, m.getLastName());
				prepareStatementMaire.setString(3, m.getCivility());

				/* From String type to Date Type */
				String[] olds = m.getDateOfBirth().split("/");
				String newDate = olds[2] + "-" + olds[1] + "-" + olds[0];
				prepareStatementMaire.setDate(4, Date.valueOf(newDate));

				prepareStatementMaire.setString(5, m.getCodeInsee());

				prepareStatementMaire.addBatch();

				// From Line to Commune
				Commune c = linetoCommune.apply(line);
				String previous = communes.put(c.getCodePostal(), c.getLibelle());
				if (previous == null) {
					prepareStatementCommune.setString(1, c.getCodePostal());
					prepareStatementCommune.setString(2, c.getLibelle());
					prepareStatementCommune.addBatch();

				}

				line = reader.readLine();

			}

			int[] counts1 = prepareStatementMaire.executeBatch();
			int[] counts2 = prepareStatementCommune.executeBatch();

			int countMaire = Arrays.stream(counts1).sum();
			int countCommune = Arrays.stream(counts2).sum();

			System.out.println("Rows for Maires added = " + countMaire);
			System.out.println("Rows for Communes added = " + countCommune);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static Function<String, Maire> linetoMaire = line -> {
		Maire m = new Maire();
		String[] fields = line.split(";");
		m.setCivility(fields[7]);
		m.setCodeInsee((fields[2]));
		m.setDateOfBirth(fields[8]);
		m.setLastName(fields[5]);
		m.setName(fields[6]);
		return m;
	};

	static Function<String, Commune> linetoCommune = line -> {
		Commune c = new Commune();
		String[] fields = line.split(";");
		String codeDpt = fields[0];
		if (codeDpt.length() == 1)
			codeDpt = "0" + codeDpt;

		String codeInsee = fields[2];

		if (codeInsee.length() == 1)
			codeInsee = "00" + codeInsee;
		else if (codeInsee.length() == 2)
			codeInsee = "0" + codeInsee;

		c.setCodePostal(codeDpt + codeInsee);
		c.setLibelle(fields[3]);
		return c;
	};
}
