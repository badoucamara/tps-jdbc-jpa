package org.camara.serie1.model;

public class Departement {
	
private  String codeDepartement;
private String name;
public Departement() {
}

public String getCodeDepartement() {
	return codeDepartement;
}
public void setCodeDepartement(String codeDepartement) {
	this.codeDepartement = codeDepartement;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

@Override
public String toString() {
	return "Departement [codeDepartement=" + codeDepartement + ", name=" + name + "]";
}


}
