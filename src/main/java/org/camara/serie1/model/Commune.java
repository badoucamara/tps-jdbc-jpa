package org.camara.serie1.model;

public class Commune {
	public Commune() {
		
	}
	public Commune(String libelle, String codePostal) {
		this.libelle = libelle;
		this.codePostal = codePostal;
	}
	
	private String libelle;
	private String codePostal;
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String string) {
		this.codePostal = string;
	}
	@Override
	public String toString() {
		return "Commune [libelle=" + libelle + ", codePostal=" + codePostal + "]";
	}

	
}
