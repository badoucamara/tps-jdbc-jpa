package org.camara.serie1.model;


public class Maire {

	private String lastName;
	private String firstName;
	private String civility;
	private String dateOfBirth;
	private String codeInsee;

	public Maire() {
	}

	public Maire(String lastName, String name, String civility, String dateOfBirth, String codeInsee) {
		super();
		this.lastName = lastName;
		this.firstName = name;
		this.civility = civility;
		this.dateOfBirth = dateOfBirth;
		this.codeInsee = codeInsee;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return firstName;
	}

	public void setName(String name) {
		this.firstName = name;
	}

	public String getCivility() {
		return civility;
	}

	public void setCivility(String civility) {
		this.civility = civility;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String fields) {
		this.dateOfBirth = fields;
	}

	public String getCodeInsee() {
		return codeInsee;
	}

	public void setCodeInsee(String fields) {
		this.codeInsee = fields;
	}

	@Override
	public String toString() {
		return "Maire [lastName=" + lastName + ", name=" + firstName + ", civility=" + civility + ", dateOfBirth="
				+ dateOfBirth + ", codeInsee=" + codeInsee + "]";
	}

}
