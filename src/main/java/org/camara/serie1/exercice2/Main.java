package org.camara.serie1.exercice2;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.function.Function;

import org.camara.serie1.model.Departement;

public class Main {

	public static void main(String[] args) throws SQLException, IOException {
		// TODO Auto-generated method stub

		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "jdbc-user",
				"badoucamara");
		String sql = "insert into Departement (id,nom)values (?,?)";

		PreparedStatement prepareStatement = connection.prepareStatement(sql);

		Path path = Path.of("data/Departments.csv");
		try (BufferedReader reader = Files.newBufferedReader(path);) {
			String line = reader.readLine();
			line = reader.readLine();
			while (line != null) {

				// From Line to Departement
				Departement dpt = lineToDepartment.apply(line);
				prepareStatement.setString(1, dpt.getCodeDepartement());
				prepareStatement.setString(2, dpt.getName());

				prepareStatement.addBatch();
				
				reader.readLine();
			}
			
			prepareStatement.executeBatch();
			System.out.println("ok");
		}
		

	}

	static Function<String, Departement> lineToDepartment = line -> {
		Departement d = new Departement();
		String code = line.split(";")[0];
		String nom = line.split(";")[1];

		if (code.length() == 1)
			code = "0" + code;

		d.setCodeDepartement(code);
		d.setName(nom);
		return d;

	};

}
