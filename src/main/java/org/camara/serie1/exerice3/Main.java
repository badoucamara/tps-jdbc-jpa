package org.camara.serie1.exerice3;

import java.sql.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String men = "Mme";
		String Departement = "ZS";
		int year = 1944;
		int age = 97;
		String women = "M";

		System.out.println("Nombre de maires dans la table : " + countMaire());
		System.out.println("Nombre de maires de civilit� " + men + " dans la table : " + countMaire(men));
		System.out.println("Le maire le plus ancien dans la table : " + getOldestMaire());
		System.out.println("Le nombre de population du departement " + Departement + " est "
				+ countDepartementPopulation(Departement));
		System.out.println("Le nombre de population du departement est " + countPopulation());
		System.out.println("Nombre de maires n�s en " + year + " dans la table : " + countMaire(year));
		System.out.println("Nombre de maires de civilit� " + women + " et d'�ge " + age + " : "
				+ countCivilityByAge(women, age));
	}

	public static long countMaire() {
		String sql = "select count(*) count from Maire";
		long countMaire = 0;

		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "jdbc-user",
					"badoucamara");

			Statement smt = connection.createStatement();
			ResultSet result = smt.executeQuery(sql);
			while (result.next())
				countMaire = result.getLong("count");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return countMaire;
	}

	public static long countMaire(String Civility) {

		String sql = "select count(*) count from Maire where civility='" + Civility + "'";
		long countMairee = 0;
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "jdbc-user",
					"badoucamara");
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sql);
			while (result.next()) {
				countMairee = result.getLong("count");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return countMairee;
	}

	public static String getOldestMaire() {

		String sql = "select civility, first_name, last_name, date_of_birth from Maire where date_of_birth "
				+ "= (select min(date_of_birth) from Maire)";
		String maire = "";
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "jdbc-user",
					"badoucamara");
			Statement smt = connection.createStatement();
			ResultSet result = smt.executeQuery(sql);
			while (result.next()) {
				maire = result.getString("civility") + ". " + result.getString("first_name") + " "
						+ result.getString("last_name") + " " + result.getString("date_of_birth");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return maire;
	}

	public static long countDepartementPopulation(String Departement) {

		String sql = "select sum(population) sum from Commune where id_commune like '" + Departement + "%'";
		long population = 0;
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "jdbc-user",
					"badoucamara");
			Statement smt = connection.createStatement();
			ResultSet result = smt.executeQuery(sql);
			while (result.next()) {
				population += result.getLong("sum");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return population;
	}

	public static long countPopulation() {

		String sql = "select sum(population) sum from Commune";
		long population = 0;
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "jdbc-user",
					"badoucamara");
			Statement smt = connection.createStatement();
			ResultSet result = smt.executeQuery(sql);
			while (result.next()) {
				population += result.getLong("sum");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return population;
	}

	public static long countMaire(int year) {

		String sql = "select count(*) count from Maire where date_of_birth like '" + year + "%'";
		long countMairee = 0;
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "jdbc-user",
					"badoucamara");
			Statement smt = connection.createStatement();
			ResultSet result = smt.executeQuery(sql);
			while (result.next()) {
				countMairee = result.getLong("count");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return countMairee;
	}

	public static String countCivilityByAge(String civility, int age) {

		String agee = "select datediff(now(),date_of_birth)/365 as ages from Maire";
		String ag = "select year(now()) as ages from Maire";

		String maire = "";
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "jdbc-user",
					"badoucamara");
			Statement smt = connection.createStatement();
			Statement smt1 = connection.createStatement();
			Statement smt2 = connection.createStatement();
			ResultSet result1 = smt1.executeQuery(agee);
			ResultSet result2 = smt2.executeQuery(ag);
			while (result1.next()) {
				result2.next();
				int maire1 = result1.getInt("ages");
				int year = result2.getInt("ages") - age;
				if (age == maire1) {
					String sql = "select first_name, last_name from Maire where civility='" + civility
							+ "' and date_of_birth like '" + year + "%'";
					ResultSet result = smt.executeQuery(sql);
					while (result.next()) {
						maire = civility + ". " + result.getString("first_name") + " " + result.getString("last_name")
								+ " " + age;
					}
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return maire;
	}
}
